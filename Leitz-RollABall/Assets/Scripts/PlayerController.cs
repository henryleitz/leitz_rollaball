﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public Text countText;
    public Text winText;
    // Vector3 is the 3 axes and the following line creates jump force
    public Vector3 jump;
    public float jumpForce = 2.0f;
    // creates timer
    public Text timerText;

    //determines whether the player is touching the ground
    public bool isGrounded;
    Rigidbody rb;
    private int count;
    //creates the start of the timer
    private float startTime;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText ();
        winText.text = "";
        //tells how the jump will act on the 3 axis
        jump = new Vector3(0.0f, 2.0f, 0.0f);
        //creating the timer variable
        startTime = Time.time;
    }

    //if condition is true(player on ground) player can jump
    void OnCollisionStay()
    {
        isGrounded = true;
    }
    //if condition is false(in the air) player can't jump again
    void OnCollisionExit()
    {
        isGrounded = false;
    }
    // Update is called once per frame
    void FixedUpdate ()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        //time variable
        float t = Time.time - startTime;
        //creates defintions for seconds and minutes
        string minutes = ((int)t / 60).ToString();
        string seconds = (t % 60).ToString("f0");

        //how to the text will be formatted on screen
        timerText.text = minutes + ":" + seconds;

        //these lines of code designate the space key as the jump key
        rb.AddForce (movement * speed);
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            rb.AddForce(jump * jumpForce, ForceMode.Impulse);
            isGrounded = false;
        }

    }
    void OnTriggerEnter(Collider other)
    {
       if (other.gameObject.CompareTag("Pick UP"))
       {
           other.gameObject.SetActive(false);
           count = count + 1;
           SetCountText (); 
       }
    }
    void SetCountText ()
    {
        countText.text = "Count: " + count.ToString();
        if (count>= 12)
        {
            winText.text = "You Win!";
        }
    }
}
